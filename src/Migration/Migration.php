<?php
/**
 * Created by PhpStorm.
 * User: Frank
 * Date: 8/31/2014
 * Time: 8:18 PM
 */

namespace fwallen\Migration;

use Yii;

class Migration extends \yii\db\Migration
{
    public function lookUp($table,$column,$criteria = null, $params=[])
    {

        $query = Yii::$app->db->createCommand("select `$column` from `$table`".(($criteria) ? " where $criteria" : null),$params);

        return $query->queryScalar();
    }
}